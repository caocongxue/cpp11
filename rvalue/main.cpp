#include <iostream>
#include <vector>
#include <algorithm>
#include <typeinfo>
// C++11新特性之右值、右值引用、移动语义、完美转发详解


// https://segmentfault.com/a/1190000016041544
// https://blog.csdn.net/weixin_43908419/article/details/130869015
// https://www.jianshu.com/p/2bf804a03a6a

class Test
{
public:
    Test(int a) : x(a){};
public:
    int x;
};

class Test getTest()
{
    class Test tmp(10);
    return tmp;
}

int main(int argc, char *argv[])
{
    int i = 10; //i 是左值 10时右值
    char c = 'a'; //c是左值 'a' 是右值
    class Test a = getTest(); //a是左值getTest返回值是右值（临时变量）
    return 0;
}

